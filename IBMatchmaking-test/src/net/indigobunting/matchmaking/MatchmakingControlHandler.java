/**
 *  MatchmakingControlHandler
 *  Indigo Bunting
 *
 *  Created by Andrey Shlapkov on 26.04.2015. 
 */

package net.indigobunting.matchmaking;

import java.util.List;
import net.indigobunting.matchmaking.classes.APlayer;
import net.indigobunting.matchmaking.classes.WaitQueue;
import net.indigobunting.matchmaking.events.ICreateGameListener;
import net.indigobunting.matchmaking.events.IRemovePlayersAfterLongWait;
import net.indigobunting.matchmaking.events.MatchmakingEventControl;

public class MatchmakingControlHandler implements ICreateGameListener, IRemovePlayersAfterLongWait
{
    @Override
    public void onCreateNewGame(int level, WaitQueue wq, List<APlayer> aPlayers)
    {
        System.out.println("New game create! Level: " + String.valueOf(level) 
                + ", Type: " + wq.GetGame().TypeGame.toString());
        
    }
    
    public void onRemovePlayersAfterLongWait(List<APlayer> aPlayers)
    {
        System.out.println("Remove players size: " + aPlayers.size());
    }
}
