package net.indigobunting.matchmaking.test;

import net.indigobunting.matchmaking.classes.APlayer;

public class Player extends APlayer 
{
    public Player(int userId, String name, int balanceWeight, long addingTimestamp) {
        super(userId, name, balanceWeight, addingTimestamp);
    } 
}
