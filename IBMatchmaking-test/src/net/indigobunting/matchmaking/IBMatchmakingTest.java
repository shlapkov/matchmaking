/**
 *  IBMatchmakingTest
 *  Indigo Bunting
 *
 *  Created by Andrey Shlapkov on 25.04.2015. 
 */

package net.indigobunting.matchmaking;

import static java.lang.Thread.sleep;
import java.util.Random;
import java.util.Scanner;
import net.indigobunting.matchmaking.classes.*;
import net.indigobunting.matchmaking.events.MatchmakingEventControl;
import net.indigobunting.matchmaking.test.Map;
import net.indigobunting.matchmaking.test.Player;
import net.indigobunting.net.matchmaking.core.BalanceController;

public class IBMatchmakingTest
{
    public static void main(String[] args)
    {
        Map gameMap = new Map(1, "Island02");
        
        Game[] games = new Game[3];
        games[0] = new Game(Game.Mode.DM, gameMap, 10);
        games[1] = new Game(Game.Mode.TDM, gameMap, 10);
        games[2] = new Game(Game.Mode.TDM, gameMap, 15);
        
        BalanceController balanceController = new BalanceController(games); 
        MatchmakingControlHandler handler = new MatchmakingControlHandler();
        balanceController.AddEventListener(handler);
        
        Scanner in = new Scanner(System.in);
        Random rand = new Random();
        
        while (true)
        {
            //TODO: ввод команд
            int i = in.nextInt();     
            int randPlayerNum = rand.nextInt();
            int randWeight = rand.nextInt(100);
            
            Player pl;
            
            switch (i) 
            {
                case 0:
                    pl = new Player(randPlayerNum, "Player_" + String.valueOf(randPlayerNum), 
                            randWeight, System.currentTimeMillis());
                    balanceController.AddPlayer(pl, 1, Game.Mode.DM);
                    break;
                case 1:
                    pl = new Player(randPlayerNum, "Player_" + String.valueOf(randPlayerNum), 
                            randWeight, System.currentTimeMillis());
                    balanceController.AddPlayer(pl, 1, Game.Mode.TDM);
                    break;
                    
                case 2:
                    System.out.println(balanceController.GetDump());
                    
                    break;
            }
        }
    }    
}
