/**
 *  BalanceController
 *  Indigo Bunting
 *
 *  Created by Andrey Shlapkov on 25.04.2015. 
 */

package net.indigobunting.net.matchmaking.core;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.indigobunting.matchmaking.classes.*;
import net.indigobunting.matchmaking.events.ICreateGameListener;
import net.indigobunting.matchmaking.events.MatchmakingEventControl;

public class BalanceController extends Thread
{   
    private Logic logic;
    private MatchmakingEventControl eventControl;
    private CopyOnWriteArrayList<WaitQueue> waitList;
    private Config cfg;
    
    private volatile boolean mIsStopped = false;
    
    //This properties loading from configuration file
    private int freqTimer;
    private int maxMediumWaitTime;
    private int maxLongWaitTime;
    
    private Logger log;
    
    public BalanceController(Game[] games)
    {
        log = Logger.getLogger(BalanceController.class.getName());
        
        cfg = new Config();
        freqTimer = Integer.valueOf(cfg.getProperty("TimerFrequency"));
        maxMediumWaitTime = Integer.valueOf(cfg.getProperty("MaxMediumWaitTime"));
        maxLongWaitTime = Integer.valueOf(cfg.getProperty("MaxLongWaitTime"));
        
        eventControl = new MatchmakingEventControl();
        waitList = new CopyOnWriteArrayList<WaitQueue>();
        for (int i = 0; i < games.length; i++)
        {
            waitList.add(new WaitQueue(games[i]));
        }
        
        this.start();
        logic = new Logic(this);
        
        log.log(Level.INFO, "Init balance controller");
    }
    
    @Override
    public void run()
    {
        while (!mIsStopped) 
        {
            //Main loop
            for (WaitQueue wq : waitList)
            {
                switch (wq.GetGame().TypeGame)
                {
                    case DM:
                        logic.DeathMatchLogic(wq);
                        break;
                        
                    case TDM:
                        logic.TeamDeathMatchLogic(wq);
                        break;
                }
            }
            
            try
            {
                sleep(freqTimer);
            } 
            catch (InterruptedException ex)
            {
                log.log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Stop thread
     */
    public void StopThis()
    {
        this.mIsStopped = true;
        
        log.log(Level.INFO,"Stop balance controller");
    }
    
    /**
     * Add player to waiting list
     * @param pl
     * @param gm 
     */
    public void AddPlayer (APlayer pl, int mapId, Game.Mode gm)
    {
        WaitQueue wq = getWQByParams(mapId, gm);
        if (wq != null)
        {
            wq.AddPlayer(pl);
            
            Formatter f = new Formatter();
            f.format("Add new player. Id: %d, Name: %s", pl.GetUserId(), pl.GetName());
            log.log(Level.INFO, f.toString());
        }
    }
    
    /**
     * get wait queue by params
     * @param mapId
     * @param gm
     * @return 
     */
    public WaitQueue getWQByParams(int mapId, Game.Mode gm)
    {
        for (WaitQueue wq : waitList)
        {
            if ((wq.GetGame().TypeGame == gm) && (wq.GetGame().GetMap().GetMapId() == mapId))
            {
                return wq;
            }
        }
        
        return null;
    }
    
    /**
     * get wait queue by params
     * @param mapName
     * @param gm
     * @return 
     */
    public WaitQueue getWQByParams(String mapName, Game.Mode gm)
    {
        for (WaitQueue wq : waitList)
        {
            if ((wq.GetGame().TypeGame == gm) && (wq.GetGame().GetMap().GetMapName()== mapName))
            {
                return wq;
            }
        }
        
        return null;
    }
    
    /**
     * Get WaitQueue by game mode
     * @param gm
     * @return 
     */
    private WaitQueue getFirstWQByGameMode (Game.Mode gm)
    {
        for (WaitQueue wq : waitList)
        {
            if (wq.GetGame().TypeGame == gm)
            {
                return wq;
            }
        }
        
        return null;
    }
    
    /**
     * Create new game
     * @param aPlayers
     * @param game 
     */
    public void CreateNewGameEvent (int level, WaitQueue wq, List<APlayer> aPlayers)
    {
        eventControl.onCreateGame(level, wq, aPlayers);
        
        Formatter f = new Formatter();
        f.format("Create new game. Level: %d, Map: %s, Game type: %s. Count players: %d",
                level, wq.GetGame().GetMap().GetMapName().toString(), 
                wq.GetGame().TypeGame.toString(), aPlayers.size());
        log.log(Level.INFO, f.toString());
    }
    
    /**
     * Remove playhers after long wait
     * @param aPlayers 
     */
    public void RemovePlayersAfterLongWaitEvent(List<APlayer> aPlayers)
    {
        eventControl.onRemovePlayersAfterLongWait(aPlayers);
        
        Formatter f = new Formatter();
        f.format("Remove players after long wait event. Count players: %s", aPlayers.size());
        log.log(Level.INFO, f.toString());
    }
    
    /**
     * Remove players from all queue
     * @param aPlayers 
     */
    public void RemovePlayersFromAllQueue(List<APlayer> aPlayers)
    {
        for (WaitQueue wq : waitList)
        {
            wq.RemoveWaitingPlayers(aPlayers);
        }
    }
    
    /**
     * Get Dump string
     * @return 
     */
    public String GetDump()
    {
        int allPlayersCount = GetAllWaitPlayersCount();
        int countPlayersInDM = GetWaitPlayersCountInDeathMatch();
        int countPlayersInTDM = GetWaitPlayersCountInTeamDeathMatch();
        
        Formatter dump = new Formatter(); 
        dump.format("All wait players: %d, \nDM count: %d, TDM count: %d", 
                allPlayersCount, countPlayersInDM, countPlayersInTDM);
        
        return dump.toString();
    }
    
    /**
     * GetAllWaitPlayersCount
     * @return 
     */
    public int GetAllWaitPlayersCount()
    {
        int count = 0;
        for (WaitQueue wq : waitList)
        {
            count += wq.GetWaitingPlayers().size();
        }
        
        return count;
    }
    
    /**
     * GetWaitPlayersCountInDeathMatch
     * @return 
     */
    public int GetWaitPlayersCountInDeathMatch()
    {
        int count = 0;
        for (WaitQueue wq : waitList)
        {
            if (wq.GetGame().TypeGame == Game.Mode.DM)
            {
                count += wq.GetWaitingPlayers().size();
            }
        }
        
        return count;
    }
    
    /**
     * GetWaitPlayersCountInTeamDeathMatch
     * @return 
     */
    public int GetWaitPlayersCountInTeamDeathMatch()
    {
        int count = 0;
        for (WaitQueue wq : waitList)
        {
            if (wq.GetGame().TypeGame == Game.Mode.TDM)
            {
                count += wq.GetWaitingPlayers().size();
            }
        }
        
        return count;
    }
    
    //Properties
    public int GetMaxMediumWaitTime()
    {
        return this.maxMediumWaitTime;
    }
    
    public int GetMaxLongWaitTime()
    {
        return this.maxLongWaitTime;
    }
    
    //Events
    public void AddEventListener(ICreateGameListener lister)
    {
        this.eventControl.addListener(lister);
    }
    
    public void RemoveEventListener(ICreateGameListener lister)
    {
        this.eventControl.removeListener(lister);
    }
}

