/**
 *  SmartFoxIntegration
 *  Indigo Bunting
 *
 *  Created by Andrey Shlapkov on 06.05.2015. 
 */

package net.indigobunting.matchmaking.smartfoxserver;

import com.smartfoxserver.v2.SmartFoxServer;
import com.smartfoxserver.v2.entities.Zone;
import com.smartfoxserver.v2.api.CreateRoomSettings;
import com.smartfoxserver.v2.api.ISFSApi;
import com.smartfoxserver.v2.api.ISFSGameApi;
import com.smartfoxserver.v2.api.SFSApi;
import com.smartfoxserver.v2.api.SFSGameApi;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.exceptions.SFSCreateRoomException;
import com.smartfoxserver.v2.exceptions.SFSJoinRoomException;
import com.smartfoxserver.v2.game.CreateSFSGameSettings;
import java.util.List;
import net.indigobunting.matchmaking.classes.APlayer;
import net.indigobunting.matchmaking.classes.Config;

public class SmartFoxIntegration
{
    private Config cfg;
    private Zone sfsZone;
    private SmartFoxServer smartFoxServerInstance;    
    private ISFSGameApi sfsGameApi;
    private ISFSApi sfsApi;
    
    //Properties from configuration files
    private String prefixRoomName;
    
    public SmartFoxIntegration(SmartFoxServer sfs, Zone sfsZone)
    {
        this.sfsZone = sfsZone;
        this.smartFoxServerInstance = sfs;
        sfsGameApi = new SFSGameApi(smartFoxServerInstance);
        sfsApi = new SFSApi(smartFoxServerInstance);
        
        this.cfg = new Config();
        prefixRoomName = cfg.getProperty("RoomNamePrefix");
    }
    
    public void CreateGame (List<APlayer> players) throws SFSCreateRoomException, SFSJoinRoomException
    {
        CreateSFSGameSettings settings = new CreateSFSGameSettings();
        settings.setName(prefixRoomName + System.currentTimeMillis());
        settings.setPassword("");
        settings.setMaxUsers(players.size());
        settings.setGame(true);
        
        Room room = sfsGameApi.createGame(sfsZone, settings, null);
        
        for (APlayer pl : players)
        {
            User u = GetUserById(pl.GetUserId());
            if (u != null)
            {
                quickJoinGame(u, room);
            }
        }
    }
    
    public void CreateGame (List<APlayer> players, boolean fireClientEvent, boolean fireServerEvent) throws SFSCreateRoomException, SFSJoinRoomException
    {
        CreateSFSGameSettings settings = new CreateSFSGameSettings();
        settings.setName(prefixRoomName + System.currentTimeMillis());
        settings.setPassword("");
        settings.setMaxUsers(players.size());
        settings.setGame(true);
        
        Room room = sfsGameApi.createGame(sfsZone, settings, null, fireClientEvent, fireServerEvent);
        
        for (APlayer pl : players)
        {
            User u = GetUserById(pl.GetUserId());
            if (u != null)
            {
                quickJoinGame(u, room, fireClientEvent, fireServerEvent);
            }
        }
    }
    
    public User GetUserById(int userId)
    {
        return sfsZone.getUserById(userId);
    }
    
    private void quickJoinGame(User player, Room room) throws SFSJoinRoomException 
    {
        sfsApi.joinRoom(player, room);
    }
    
    private void quickJoinGame(User player, Room room, boolean fireClientEvent, boolean fireServerEvent) 
            throws SFSJoinRoomException 
    {
        sfsApi.joinRoom(player, room, "", false, null, fireClientEvent, fireServerEvent);
    }
}
