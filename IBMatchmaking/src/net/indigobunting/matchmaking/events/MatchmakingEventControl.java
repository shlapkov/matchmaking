/**
 *  MatchmakingControl
 *  Indigo Bunting
 *
 *  Created by Andrey Shlapkov on 26.04.2015. 
 */

package net.indigobunting.matchmaking.events;

import java.util.ArrayList;
import java.util.List;
import net.indigobunting.matchmaking.classes.APlayer;
import net.indigobunting.matchmaking.classes.WaitQueue;

public class MatchmakingEventControl
{
    private ArrayList<ICreateGameListener> createGameListeners = new ArrayList<ICreateGameListener>();
    private ArrayList<IRemovePlayersAfterLongWait> removePlayersAfterLongWaitsListeners 
            = new ArrayList<IRemovePlayersAfterLongWait>();
    
    public void addListener(ICreateGameListener listener)
    {
        createGameListeners.add(listener);
    }

    public void removeListener(ICreateGameListener listener)
    {
        createGameListeners.add(listener);
    }
    
    public void addListener(IRemovePlayersAfterLongWait listener)
    {
        removePlayersAfterLongWaitsListeners.add(listener);
    }

    public void removeListener(IRemovePlayersAfterLongWait listener)
    {
        removePlayersAfterLongWaitsListeners.add(listener);
    }
    
    public void onCreateGame(int level, WaitQueue wq, List<APlayer> aPlayers) 
    {
        for(ICreateGameListener listener : createGameListeners)
        {
            listener.onCreateNewGame(level, wq, aPlayers);
        }
    }
    
    public void onRemovePlayersAfterLongWait(List<APlayer> aPlayers)
    {
        for(IRemovePlayersAfterLongWait listener : removePlayersAfterLongWaitsListeners)
        {
            listener.onRemovePlayersAfterLongWait(aPlayers);
        }
    }
}
