/**
 *  MatchmakingControlListener
 *  Indigo Bunting
 *
 *  Created by Andrey Shlapkov on 26.04.2015. 
 */

package net.indigobunting.matchmaking.events;

import java.util.List;
import net.indigobunting.matchmaking.classes.APlayer;
import net.indigobunting.matchmaking.classes.WaitQueue;

public interface ICreateGameListener 
{
    void onCreateNewGame(int level, WaitQueue wq, List<APlayer> aPlayers);
}
