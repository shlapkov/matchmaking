/**
 *  IRemovePlayerAfterLongWait
 *  Indigo Bunting
 *
 *  Created by Andrey Shlapkov on 06.05.2015. 
 */

package net.indigobunting.matchmaking.events;

import java.util.List;
import net.indigobunting.matchmaking.classes.APlayer;
import net.indigobunting.matchmaking.classes.WaitQueue;

public interface IRemovePlayersAfterLongWait 
{
    void onRemovePlayersAfterLongWait(List<APlayer> aPlayers);
}
