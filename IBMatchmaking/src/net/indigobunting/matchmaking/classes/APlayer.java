/**
 *  APlayer
 *  Indigo Bunting
 *
 *  Created by Andrey Shlapkov on 25.04.2015. 
 */

package net.indigobunting.matchmaking.classes;

public abstract class APlayer
{
    protected int userId;
    protected String name;
    protected int balanceWeight;
    protected long addingTimestamp;
    
    public APlayer (int userId, String name, int balanceWeight, long addingTimestamp)
    {
        this.userId = userId;
        this.name = name;
        this.balanceWeight = balanceWeight;
        this.addingTimestamp = addingTimestamp;
    }
    
    //Properties
    public int GetUserId()
    {
        return this.userId;
    }
    
    public String GetName()
    {
        return this.name;
    }
    
    public int GetBalanceWeight()
    {
        return this.balanceWeight;
    }
    
    public long GetAddingTimestamp()
    {
        return this.addingTimestamp;
    }
    
}
