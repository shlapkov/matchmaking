/**
 *  GameMode
 *  Indigo Bunting
 *
 *  Created by Andrey Shlapkov on 25.04.2015. 
 */

package net.indigobunting.matchmaking.classes;

import java.util.List;

public class Game 
{
    private int maxPlayers;
    
    public Mode TypeGame;
    
    private AMap map;
    
    public Game (Mode m, AMap map, int maxPlayers)
    {
        this.TypeGame = m;
        this.maxPlayers = maxPlayers;
        this.map = map;
    }
    
    public int GetMaxPlayers()
    {
        return this.maxPlayers;
    }
    
    public AMap GetMap()
    {
        return this.map;
    }
    
    public enum Mode { ALL, DM, TDM, TE }
}
