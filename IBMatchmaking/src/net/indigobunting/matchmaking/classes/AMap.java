/**
 *  AMap
 *  Indigo Bunting
 *
 *  Created by Andrey Shlapkov on 06.05.2015. 
 */

package net.indigobunting.matchmaking.classes;

public abstract class AMap
{
    private int mapId;
    private String mapName;
    
    public AMap(int mapId, String name)
    {
        this.mapId = mapId;
        this.mapName = name;
    }
    
    public int GetMapId()
    {
        return this.mapId;
    }
    
    public String GetMapName()
    {
        return this.mapName;
    }
}
