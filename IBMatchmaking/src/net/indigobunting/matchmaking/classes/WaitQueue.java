/**
 *  WaitQueue
 *  Indigo Bunting
 *
 *  Created by Andrey Shlapkov on 25.04.2015. 
 */

package net.indigobunting.matchmaking.classes;

import java.util.ArrayList;
import java.util.List;

public class WaitQueue 
{
    private List<APlayer> waitingPlayers;
    private Game game;
    
    public WaitQueue (Game g)
    {
        this.game = g;
        this.waitingPlayers = new ArrayList<APlayer>();
    }
    
    public void AddPlayer (APlayer pl)
    {
        if (GetPlayersByUserId(pl.GetUserId()) == null)
        {
            this.waitingPlayers.add(pl);
        }
    }
    
    public List<APlayer> GetWaitingPlayers()
    {
        return this.waitingPlayers;
    }
    
    public void RemoveWaitingPlayers (List<APlayer> removeList)
    {
        this.waitingPlayers.removeAll(removeList);
    }
    
    public APlayer GetPlayersByUserId(int playerId)
    {
        for (APlayer pl : waitingPlayers)
        {
            if (pl.GetUserId() == playerId)
            {
                return pl;
            }
        }
        
        return null;
    }
    
    public Game GetGame()
    {
        return this.game;
    }
    
}


