/**
 *  Logic
 *  Indigo Bunting
 *
 *  Created by Andrey Shlapkov on 25.04.2015. 
 */

package net.indigobunting.matchmaking.classes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import net.indigobunting.net.matchmaking.core.BalanceController;


public class Logic 
{
    private BalanceController balanceController;

    public Logic (BalanceController bc)
    {
        this.balanceController = bc;
    }
    
    public void DeathMatchLogic (WaitQueue wq)
    {
        //sort by adding timestamp
        List<APlayer> waitingPlayers = wq.GetWaitingPlayers();
        sortPlayersListByTimestamp(waitingPlayers);
        
        //for 10 graduation
        for (int i = 0; i < 10; i++)
        {
            int level = i + 1;
            int gradFrom = i * 10;
            int gradTo = i * 10 + 10;
            
            List<APlayer> suitablePlayers = new ArrayList<APlayer>();  
            List<APlayer> removePlayers = new ArrayList<APlayer>();
            
            for (APlayer pl : waitingPlayers)
            {
                int plBalanceWeight = pl.GetBalanceWeight();
                if ((plBalanceWeight >= gradFrom && plBalanceWeight < gradTo) && !removePlayers.contains(pl))
                {
                    suitablePlayers.add(pl);
                }
                
                if (suitablePlayers.size() == wq.GetGame().GetMaxPlayers())
                {
                    balanceController.CreateNewGameEvent(level, wq, suitablePlayers);
                    removePlayers.addAll(suitablePlayers);
                    suitablePlayers = new ArrayList<APlayer>();
                }
            }
            
            if (removePlayers.size() > 0)
            {
                balanceController.RemovePlayersFromAllQueue(removePlayers);
            }
        }
        
        //remove players with high expectations
        List <APlayer> removePlayersLongWait = new ArrayList<APlayer>();
        for (APlayer pl : removePlayersLongWait)
        {
            if (checkLongWait(pl))
            {
                removePlayersLongWait.add(pl);
            }
        }
        
        balanceController.RemovePlayersFromAllQueue(removePlayersLongWait);
    }
    
    public void TeamDeathMatchLogic (WaitQueue wq)
    {
        //for 10 graduation
        for (int i = 0; i < 10; i++)
        {
            //sort by adding timestamp
            List<APlayer> waitingPlayers = wq.GetWaitingPlayers();
            sortPlayersListByTimestamp(waitingPlayers);
            
            int level = i + 1;
            int gradFrom = i * 10;
            int gradTo = i * 10 + 10;
            
            List<APlayer> suitablePlayers = new ArrayList<APlayer>();  
            List<APlayer> removePlayers = new ArrayList<APlayer>();
            
            for (APlayer pl : waitingPlayers)
            {
                int plBalanceWeight = pl.GetBalanceWeight();
                if ((plBalanceWeight >= gradFrom && plBalanceWeight < gradTo) 
                        && !removePlayers.contains(pl))
                {
                    suitablePlayers.add(pl);
                }
                
                if (suitablePlayers.size() == wq.GetGame().GetMaxPlayers())
                {
                    sortPlayersListByBalanceWeight(suitablePlayers);
                    
                    List<APlayer> firstTeam = new ArrayList<APlayer>();
                    List<APlayer> secondTeam = new ArrayList<APlayer>();
                    
                    for (int j = 0; j < suitablePlayers.size(); j++)
                    {
                        if (j % 2 == 0)
                        {
                            firstTeam.add(suitablePlayers.get(j));
                        } 
                        else
                        {
                            secondTeam.add(suitablePlayers.get(j));
                        }
                    }
                    
                    if (checkCorrentTeams(firstTeam, secondTeam))
                    {
                        balanceController.CreateNewGameEvent(level, wq, suitablePlayers);
                        removePlayers.addAll(suitablePlayers);
                        suitablePlayers = new ArrayList<APlayer>();
                    } 
                    else
                    {
                        suitablePlayers = new ArrayList<APlayer>();
                    }
                }
            }
            
            if (removePlayers.size() > 0)
            {
                wq.RemoveWaitingPlayers(removePlayers);
                removePlayers = new ArrayList<APlayer>();
            }
        }
        
        //increase the range
        for (int i = 0; i < 10; i++)
        {
            //sort by adding timestamp
            List<APlayer> waitingPlayers = wq.GetWaitingPlayers();
            sortPlayersListByTimestamp(waitingPlayers);
            
            int level = i + 1;
            int gradFrom = (i > 1) ? ((i - 1) * 10) : 0;
            int gradTo = (i < 10) ? ((i + 1) * 10 + 10) : 100;
            
            List<APlayer> suitablePlayers = new ArrayList<APlayer>();  
            List<APlayer> removePlayers = new ArrayList<APlayer>();
            
            for (APlayer pl : waitingPlayers)
            {
                int plBalanceWeight = pl.GetBalanceWeight();
                if ((plBalanceWeight >= gradFrom && plBalanceWeight < gradTo) && !removePlayers.contains(pl))
                {
                    suitablePlayers.add(pl);
                }
                
                if (suitablePlayers.size() == wq.GetGame().GetMaxPlayers())
                {
                    sortPlayersListByBalanceWeight(suitablePlayers);
                    
                    List<APlayer> firstTeam = new ArrayList<APlayer>();
                    List<APlayer> secondTeam = new ArrayList<APlayer>();
                    
                    for (int j = 0; j < suitablePlayers.size(); j++)
                    {
                        if (j % 2 == 0)
                        {
                            firstTeam.add(suitablePlayers.get(j));
                        } 
                        else
                        {
                            secondTeam.add(suitablePlayers.get(j));
                        }
                    }
                    
                    if (checkCorrentTeams(firstTeam, secondTeam))
                    {
                        balanceController.CreateNewGameEvent(level, wq, suitablePlayers);
                        removePlayers.addAll(suitablePlayers);
                        suitablePlayers = new ArrayList<APlayer>();
                    } 
                    else
                    {
                        suitablePlayers = new ArrayList<APlayer>();
                    }
                }
            }
            
            if (removePlayers.size() > 0)
            {
                balanceController.RemovePlayersFromAllQueue(removePlayers);
                removePlayers = new ArrayList<APlayer>();
            }
        }
        
        //incomplete teams (priority allocation)
        for (int i = 0; i < 10; i+=2)
        {
            //sort by adding timestamp
            List<APlayer> waitingPlayers = wq.GetWaitingPlayers();
            sortPlayersListByTimestamp(waitingPlayers);
            
            int level = i + 1;
            int gradFrom = (i > 1) ? ((i - 1) * 10) : 0;
            int gradTo = (i < 10) ? ((i + 1) * 10 + 10) : 100;
            
            List<APlayer> suitablePlayers = new ArrayList<APlayer>();  
            List<APlayer> removePlayers = new ArrayList<APlayer>();
            
            for (APlayer pl : waitingPlayers)
            {
                int plBalanceWeight = pl.GetBalanceWeight();
                if ((plBalanceWeight >= gradFrom && plBalanceWeight < gradTo) 
                        && !removePlayers.contains(pl) && checkMediumWait(pl))
                {
                    suitablePlayers.add(pl);
                }
            }
            
            if (suitablePlayers.size() > 1)
            {
                int countTeam = (int)Math.ceil((double)suitablePlayers.size() / (double)wq.GetGame().GetMaxPlayers());
                int countPlayersInGame = (int)Math.ceil((double)suitablePlayers.size() / (double)countTeam);
                for (int j = 0; j < countTeam; j++)
                {
                    sortPlayersListByBalanceWeight(suitablePlayers);

                    List<APlayer> firstTeam = new ArrayList<APlayer>();
                    List<APlayer> secondTeam = new ArrayList<APlayer>();

                    System.out.println("Count team: " + countPlayersInGame);
                    
                    for (int k = 0; k < countPlayersInGame; k++)
                    {
                        if (k % 2 == 0)
                        {
                            firstTeam.add(suitablePlayers.get(k));
                        } 
                        else
                        {
                            secondTeam.add(suitablePlayers.get(k));
                        }
                    }

                    List<APlayer> readyPlayers = new ArrayList<APlayer>();
                    for (APlayer pl : firstTeam)
                    {
                        readyPlayers.add(pl);
                    }             
                    for (APlayer pl : secondTeam)
                    {
                        readyPlayers.add(pl);
                    }

                    balanceController.CreateNewGameEvent(level, wq, readyPlayers);
                    suitablePlayers.removeAll(readyPlayers);
                    removePlayers.addAll(readyPlayers);
                }

                if (removePlayers.size() > 0)
                {
                    balanceController.RemovePlayersFromAllQueue(removePlayers);
                    removePlayers = new ArrayList<APlayer>();
                }
            }
        }
                 
        //remove players with high expectations
        List <APlayer> removePlayersLongWait = new ArrayList<APlayer>();
        for (APlayer pl : removePlayersLongWait)
        {
            if (checkLongWait(pl))
            {
                removePlayersLongWait.add(pl);
            }
        }
        
        balanceController.RemovePlayersFromAllQueue(removePlayersLongWait);
    }
    
    private void sortPlayersListByTimestamp(List<APlayer> waitingPlayers)
    {
        Collections.sort(waitingPlayers, new Comparator<APlayer>(){
            public int compare(APlayer s1, APlayer s2) {
                return (s1.GetAddingTimestamp() > s2.GetAddingTimestamp()) ? -1 : 1;
            }
        });
    }
    
    private void sortPlayersListByBalanceWeight(List<APlayer> waitingPlayers)
    {
        Collections.sort(waitingPlayers, new Comparator<APlayer>(){
            public int compare(APlayer s1, APlayer s2) {
                return (s1.GetBalanceWeight() > s2.GetBalanceWeight()) ? 1 : -1;
            }
        });
    }
    
    private boolean checkMediumWait(APlayer pl)
    {
        long currentTimestamp = System.currentTimeMillis();
        if (currentTimestamp - pl.GetAddingTimestamp() >= this.balanceController.GetMaxMediumWaitTime())
        {
            return true;
        }
        
        return false;
    }
    
    private boolean checkLongWait(APlayer pl)
    {
        long currentTimestamp = System.currentTimeMillis();
        if (currentTimestamp - pl.GetAddingTimestamp() >= this.balanceController.GetMaxLongWaitTime())
        {
            return true;
        }
        
        return false;
    }
    
    private boolean checkCorrentTeams (List<APlayer> firstTeam, List<APlayer> secondTeam)
    {
        boolean isCorrect = false;
        
        int firstWeight = getBalanceWeightTeam(firstTeam);
        int secondWeight = getBalanceWeightTeam(secondTeam);
        
        float firstPer = 100.0f;
        float secondPer = (secondWeight * firstPer) / firstWeight;
        
        System.out.println("First team weight: " + firstWeight + ", second team weight: " + secondWeight);
        
        if (Math.abs(firstPer - secondPer) <= 10)
        {
            isCorrect = true;
        }
        
        return isCorrect;
    }
    
    private int getBalanceWeightTeam (List<APlayer> players)
    {
        int bWeight = 0;
        
        for (APlayer pl : players)
        {
            bWeight += pl.GetBalanceWeight();
        }
        
        return bWeight;
    }
}
